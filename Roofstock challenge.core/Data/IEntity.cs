﻿namespace Roofstock_challenge.core.Data
{
    /// <summary>
    /// Parent Entity Interface.
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
