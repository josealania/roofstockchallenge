﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roofstock_challenge.core.Data
{
    /// <summary>
    /// Generic Repository Interface CRUD.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class, IEntity
    {
        /// <summary>
        /// To Read All Entities.
        /// </summary>
        /// <returns>All Entities.</returns>
        Task<List<T>> GetAll();
        
        /// <summary>
        /// To Read one Emtity by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>One Entity that matches with parameter id.</returns>
        Task<T> Get(int id);
        
        /// <summary>
        /// To Create one Entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Created Entity.</returns>
        Task<T> Add(T entity);
        
        /// <summary>
        /// To Update one Entity by id.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated Entity by id.</returns>
        Task<T> Update(T entity);
        
        /// <summary>
        /// To Delete one Entity by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted Entity by id.</returns>
        Task<T> Delete(int id);
    }
}
