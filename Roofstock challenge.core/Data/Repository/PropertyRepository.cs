﻿using Microsoft.Extensions.Logging;
using Roofstock_challenge.core.Models;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// Property Repository class.
    /// </summary>
    public class PropertyRepository : EfCoreRepository<Property, Roofstock_challengeContextSQL>
    {
        /// <summary>
        /// Constructor to inject logger and context.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="context"></param>
        public PropertyRepository(ILoggerFactory logger,Roofstock_challengeContextSQL context) : base(logger,context)
        {

        }
        // We can add new methods specific to the property repository here in the future.
    }
}
