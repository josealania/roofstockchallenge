﻿using Roofstock_challenge.core.DTOs;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// Property DTO Repository.
    /// </summary>
    public class PropertyDTORepository : CoreDTORepository<DTOProperty>
    {        
        /// <summary>
        /// Constructor to inject context.
        /// </summary>
        /// <param name="context"></param>
        public PropertyDTORepository(APIDSContext context):base(context)
        {

        }
        // We can add new methods specific to the property DTO repository here in the future.
    }
}
