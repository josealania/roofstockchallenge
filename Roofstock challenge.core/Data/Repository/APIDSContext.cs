﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// API Data Source Context
    /// </summary>
    public class APIDSContext
    {
        private readonly IConfiguration _configuration;
        public readonly string baseUrl;
        private readonly ILoggerFactory _loggerFactory;
        
        /// <summary>
        /// Constructor to inject loggerFactory and configuration.
        /// </summary>
        /// <param name="loggerFactory"></param>
        /// <param name="configuration"></param>
        public APIDSContext(ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            this._configuration = configuration;
            this._loggerFactory = loggerFactory;
            baseUrl = this._configuration.GetSection("WebServicesSettings")["BaseUrl"];
        }

        /// <summary>
        /// To Set the generic type to ApiSet.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public ApiSet<TEntity> Set<TEntity>() where TEntity : class
        {
            var result = new ApiSet<TEntity>(this, _loggerFactory);
            return result;
        }
    }
}
