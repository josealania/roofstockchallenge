﻿using Microsoft.EntityFrameworkCore;
using Roofstock_challenge.core.Models;

namespace Roofstock_challenge.core.Data
{
    /// <summary>
    /// Entity Framework Context Setup.
    /// </summary>
    public class Roofstock_challengeContextSQL : DbContext
    {
        /// <summary>
        /// Class Constructor to inject context options.
        /// </summary>
        /// <param name="options"></param>
        public Roofstock_challengeContextSQL (DbContextOptions<Roofstock_challengeContextSQL> options)
            : base(options)
        {
        }
        #region ORM Mapppings
        public DbSet<Property> Properties { get; set; }
        #endregion
    }
}
