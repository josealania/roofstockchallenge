﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Roofstock_challenge.core.DTOs;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// Data Structure to deal with Api collections.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class ApiSet<TEntity>
    {
        private readonly APIDSContext _context;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor to inject context and loggerFactory.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        public ApiSet(APIDSContext context, ILoggerFactory logger)
        {
            this._context = context;
            this._logger = logger.CreateLogger("DTORepository");
        }

        /// <summary>
        /// To Get all entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        public async Task<List<TEntity>> GetAllAsync() 
        {
            var userJson = await GetStringAsync(_context.baseUrl);
            // Here I use Newtonsoft.Json to deserialize JSON string to User object
            try
            {
                var entitiesRoot = JsonConvert.DeserializeObject<DTOEntitiesRoot>(userJson);
                var entities = entitiesRoot.Properties;
                return (List<TEntity>)(object)entities;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// To get a string from endpoint.
        /// </summary>
        /// <param name="url"></param>
        /// <returns>String with a json content.</returns>
        private static async Task<string> GetStringAsync(string url)
        {
            using (var httpClient = new HttpClient())
            {
                return await httpClient.GetStringAsync(url);
            }
        }
    }
} 