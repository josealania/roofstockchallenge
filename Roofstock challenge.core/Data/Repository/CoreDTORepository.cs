﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// Parent DTO CRUD Repository.
    /// </summary>
    public abstract class CoreDTORepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly APIDSContext _context;

        /// <summary>
        /// Constructor to inject context.
        /// </summary>
        public CoreDTORepository(APIDSContext context)
        {
            this._context = context;
        }

        /// <summary>
        /// Create entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Created entity.</returns>
        public Task<TEntity> Add(TEntity entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted entity.</returns>
        public Task<TEntity> Delete(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read Entity by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Read Entity by id.</returns>
        public Task<TEntity> Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read all entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        public async Task<List<TEntity>> GetAll()
        {
            return await _context.Set<TEntity>().GetAllAsync();
        }

        /// <summary>
        /// Update entity by id.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated entity bi id.</returns>
        public Task<TEntity> Update(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
