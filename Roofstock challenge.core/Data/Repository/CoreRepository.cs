﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roofstock_challenge.core.Data.Repository
{
    /// <summary>
    /// Generic Parent Repositories CRUD Abstract Class.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    public abstract class EfCoreRepository<TEntity, TContext> : IRepository<TEntity>
        where TEntity : class, IEntity
        where TContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private readonly ILogger _logger;
        private readonly TContext context;

        /// <summary>
        /// Constructor to inject logger and EF context.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="context"></param>
        public EfCoreRepository(ILoggerFactory logger,TContext context)
        {
            this._logger = logger.CreateLogger("Repository");
            this.context = context;
        }

        /// <summary>
        /// Create Entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Created Emtity.</returns>
        public async Task<TEntity> Add(TEntity entity)
        {
            try
            {
                context.Set<TEntity>().Add(entity);
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete Entity.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted Entity by id.</returns>
        public async Task<TEntity> Delete(int id)
        {
            var entity = await context.Set<TEntity>().FindAsync(id);
            if (entity == null)
            {
                return entity;
            }

            context.Set<TEntity>().Remove(entity);
            await context.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Read Entity by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity found by id.</returns>
        public async Task<TEntity> Get(int id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        /// <summary>
        /// Read Entities.
        /// </summary>
        /// <returns>List of Entities.</returns>
        public async Task<List<TEntity>> GetAll()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        /// <summary>
        /// Update Entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated Entity by id.</returns>
        public async Task<TEntity> Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return entity;
        }

    }
}
