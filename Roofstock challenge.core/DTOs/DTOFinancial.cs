﻿namespace Roofstock_challenge.core.DTOs
{
    public class DTOFinancial
    {
        public decimal ListPrice { get; set; }
        public decimal MonthlyRent { get; set; }
    }
}
