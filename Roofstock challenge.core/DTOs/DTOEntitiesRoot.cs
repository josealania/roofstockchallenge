﻿using System.Collections.Generic;

namespace Roofstock_challenge.core.DTOs
{
    class DTOEntitiesRoot
    {
        public List<DTOProperty> Properties { get; set; }
    }
}
