﻿namespace Roofstock_challenge.core.DTOs
{
    public class DTOAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public int ZIP { get; set; }
        /// <summary>
        /// To concatenate a full address.
        /// </summary>
        /// <returns>String with the full address.</returns>
        public string GetFullAddress() {
            return $"{Address1}, {City} - {State}";
        }
    }
}
