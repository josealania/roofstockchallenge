﻿using Roofstock_challenge.core.Data;

namespace Roofstock_challenge.core.DTOs
{
    public class DTOProperty : IEntity
    {
        public int Id { get; set; }
        public DTOAddress Address { get; set; }
        public DTOPhysical Physical { get; set; }
        public DTOFinancial Financial { get; set; }
    }
}
