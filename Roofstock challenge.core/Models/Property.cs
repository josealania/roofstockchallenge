﻿using Roofstock_challenge.core.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Roofstock_challenge.core.Models
{
    public class Property : IEntity
    {
        public int Id { get; set; }
        [Required]
        public int DTOId { get; set; }
        [Required]
        [StringLength(150)]
        public string Address { get; set; }
        [Required]
        public int YearBuilt { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal ListPrice { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal MonthlyRent { get; set; }
    }
}
