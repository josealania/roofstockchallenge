﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Roofstock_challenge.core.Data;
using Roofstock_challenge.core.DTOs;
using Roofstock_challenge.core.Models;
using Roofstock_challenge.MappingConfigurations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roofstock_challenge.Controllers.Tests
{
    /// <summary>
    /// Properties Controller Tests.
    /// </summary>
    [TestClass()]
    public class PropertiesControllerTests
    {
        #region properties_GetAll_ReturnsAResult_WithAListOfProperties
 
        /// <summary>
        /// To return result with a list of properties.
        /// </summary>
        /// <returns>Test result.</returns>
        [TestMethod()]
        public async Task properties_GetAll_With_A_Not_Null_List()
        {
            var mock = new Mock<IRepository<Property>>();
            var mockDTO = new Mock<IRepository<DTOProperty>>();
            mockDTO.Setup(repo => repo.GetAll())
                .ReturnsAsync(GetTestDTOProperties());
            var myProfile = new UserProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            // Arrange
            var controller = new PropertiesController(mock.Object, mockDTO.Object,mapper);

            // Act
            var result = await controller.Get();

            // Assert           
            Assert.IsTrue(result!=null);
        }

        /// <summary>
        /// To test when a Not Found Result error 404.
        /// </summary>
        /// <returns>Not Found Result is true.</returns>
        [TestMethod()]
        public async Task properties_GetAll_With_A_404_Error()
        {
            // Arrange
            var controller = new PropertiesController(repository:null,repositoryDTO:null,mapper:null);

            // Act
            var result = await controller.Get();

            IActionResult actionResult = result.Result;
            var notFoundRes = actionResult as NotFoundResult;
            Assert.IsTrue(notFoundRes.StatusCode==404);
        }
        #endregion
        #region property_Save_ReturnsAResult

        /// <summary>
        /// To test Controller Add method.
        /// </summary>
        /// <returns>True if Add method worked.</returns>
        [TestMethod()]
        public async Task property_Save()
        {
            var mock = new Mock<IRepository<Property>>();
            var mockDTO = new Mock<IRepository<DTOProperty>>();
            mock.Setup(repo => repo.Add(GetTestProperty()));
            var myProfile = new UserProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            // Arrange
            var controller = new PropertiesController(mock.Object, mockDTO.Object, mapper);

            // Act
            var result = await controller.Post(GetTestProperty());

            // Assert           
            Assert.IsTrue(result != null);
        }

        /// <summary>
        /// To test Controller Add method Http 500.
        /// </summary>
        /// <returns>Error 500 is true.</returns>
        [TestMethod()]
        public async Task property_Save_With_A_500_Error()
        {
            // Arrange
            var controller = new PropertiesController(repository: null, repositoryDTO: null, mapper: null);

            // Act
            var result = await controller.Post(GetTestProperty());

            IActionResult actionResult = result.Result;
            var internalErrorResult = actionResult as StatusCodeResult;
            Assert.IsTrue(internalErrorResult.StatusCode == 500);
        }
        #endregion
        #region Property

        /// <summary>
        /// To mock a property.
        /// </summary>
        /// <returns>Mock Property.</returns>
        private Property GetTestProperty()
        {
            Property property =new Property()
            {
                Id = 2,
                  DTOId = 1234,
                Address = "Golf Street s/n",
                YearBuilt = 1984,
                ListPrice = 15.20M,
                MonthlyRent = 210.05M,
            };
            return property;
        }
        #endregion
        #region DTOProperties

        /// <summary>
        /// To mock a DTO Property.
        /// </summary>
        /// <returns>Mocked a List of DTO Properties.</returns>
        private List<DTOProperty> GetTestDTOProperties()
        {
            var properties = new List<DTOProperty>();
            properties.Add(new DTOProperty()
            {
                Id = 1001,
                Address = new DTOAddress() { Address1="Av Hermes 142 - Ate",City="Lima",State="New York" },
                Financial = new DTOFinancial() { ListPrice=123.00M, MonthlyRent=98.23M },
                Physical = new DTOPhysical() { YearBuilt=2001 }
            });
            properties.Add(new DTOProperty()
            {
                Id = 1002,
                Address = new DTOAddress() { Address1 = "Av Las Garzas 234 - Santa Anita", City = "Ica", State = "California" },
                Financial = new DTOFinancial() { ListPrice = 67.90M, MonthlyRent = 125.03M },
                Physical = new DTOPhysical() { YearBuilt = 2021 }
            });
            return properties;
        }
        #endregion
    }
}