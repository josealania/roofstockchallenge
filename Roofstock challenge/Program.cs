using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
namespace Roofstock_challenge
{
    /// <summary>
    /// The host is typically configured, built, and run by code in the Program class. 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Creates a .NET Generic Host 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Configure a Host with a set of default options.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>Configured Host.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
