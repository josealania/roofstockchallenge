import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Property } from '../models/Property';


@Injectable({
  providedIn: 'root'
})

export class PropertyService {
  url = '';
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
  }
  getAllProperty(): Observable<Property[]> {
    return this.http.get<Property[]>(this.url + 'api/properties');
  }

  saveProperty(property: Property): Observable<Property> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Property>(this.url + 'api/properties',
      property, httpOptions);
  }
}
