import { Component, ErrorHandler, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Property } from '../../models/Property';
import { PropertyService } from '../../services/manage-properties.service';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-properties',
  templateUrl: './manage-properties.component.html',
  styleUrls: ["./manage-properties.component.css"]
})
export class ManagePropertiesComponent {
  public properties: Observable<Property[]>;
  public propertySelected: Property;
  errorMessage;
  constructor(private propertyService: PropertyService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loadAllProperties();
  }
  showToaster(message) {
    this.toastr.success(message)
  }
  showErrorToaster(message) {
    this.toastr.error(message)
  }
  loadAllProperties() {
    this.errorMessage = "";
    let result = this.propertyService.getAllProperty().toPromise().then(result => {
      result = result.map(item => ({
        ...item,
        isSelected: false
      }));
      this.properties = of(result);
    }).catch(error => {
      if (error.status == 404) { this.errorMessage = "Service Not Found, please contact your technical support and give them the following errorcode=404."; }
      console.log(error);
    });
  }

  saveProperty(propertyId: number) {
    this.properties.subscribe((properties: Property[]) => {
      let thePropertySelected = properties.find(x => x.dtoId == propertyId);
      this.propertySelected = thePropertySelected;
      this.propertyService.saveProperty(this.propertySelected).toPromise().then(
        result => {
          console.log(result);
          this.showToaster('Property successfully saved to Database')
        }
      ).catch(error => {
        console.log(error);
        if (error.status == 500) {
          this.propertySelected.isSelected = false;
          this.showErrorToaster('Property not saved, contact your technical support and give them the following errorcode=500.');
        }
      });
    });
  }
}

