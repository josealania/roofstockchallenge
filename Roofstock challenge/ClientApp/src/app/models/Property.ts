export interface Property {
  dtoId: number;
  address: string;
  yearBuilt: number;
  listPrice: number;
  monthlyRent: number;
  isSelected: boolean;
}
