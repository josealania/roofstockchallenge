﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Roofstock_challenge.core.Data;
using Roofstock_challenge.core.DTOs;
using Roofstock_challenge.core.Models;

namespace Roofstock_challenge.Controllers
{
    /// <summary>
    /// Child Property Entity Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PropertiesController : BaseController<Property, DTOProperty, IRepository<Property>, IRepository<DTOProperty>>
    {
        /// <summary>
        /// Property Controller Constructor to inject repository, repositoryDTO and mapper.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryDTO"></param>
        /// <param name="mapper"></param>
        public PropertiesController(IRepository<Property> repository, IRepository<DTOProperty> repositoryDTO, IMapper mapper) : base(repository, repositoryDTO, mapper)
        {
            
        }
        // We can add new methods specific to the property controller here in the future.
    }
}
