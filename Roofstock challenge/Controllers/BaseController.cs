﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roofstock_challenge.core.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roofstock_challenge.Controllers
{
    /// <summary>
    /// Generic Parent Controller 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TEntityDTO"></typeparam>
    /// <typeparam name="TRepository"></typeparam>
    /// <typeparam name="TRepositoryDTO"></typeparam>
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseController<TEntity, TEntityDTO, TRepository, TRepositoryDTO> : ControllerBase
        where TEntity : class, IEntity
        where TRepository : IRepository<TEntity>
        where TEntityDTO : class, IEntity
        where TRepositoryDTO : IRepository<TEntityDTO>
    {
        private readonly TRepository repository;
        private readonly TRepositoryDTO repositoryDTO;
        private readonly IMapper _mapper;
        
        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="repositoryDTO"></param>
        /// <param name="mapper"></param>
        public BaseController(TRepository repository, TRepositoryDTO repositoryDTO, IMapper mapper)
        {
            this.repository = repository;
            this.repositoryDTO = repositoryDTO;
            _mapper = mapper;
        }

        /// <summary>
        /// API Method to get all Entities.
        /// </summary>
        /// <returns>List of Entities or NotFoundResult.</returns>
        // GET: api/[controller]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            try
            {
                var dtoEntity = await repositoryDTO.GetAll();
                var entities = _mapper.Map<List<TEntity>>(dtoEntity);
                return entities;

            }
            catch (Exception)
            {
                return NotFound();
                throw;
            }
            finally 
            {
            }
        }
        
        /// <summary>
        /// API Method to get one entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns>One Entity that matches with parameter id.</returns>
        // GET: api/[controller]/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TEntity>> Get(int id)
        {
            var entity = await repository.Get(id);
            if (entity == null)
            {
                return NotFound();
            }
            return entity;
        }
        
        /// <summary>
        /// API Method to Update one Entity.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns>Updated Entity or NotFoundResult or BadRequestResult.</returns>
        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TEntity>> Put(int id, TEntity entity)
        {
            if (id != entity.Id)
            {
                return BadRequest();
            }
            var entityToUpdate =await repository.Update(entity);
            if (entityToUpdate == null)
            {
                return NotFound();
            }
            return entityToUpdate;
        }
        
        /// <summary>
        /// API Method to Save one Entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Created Entity.</returns>
        // POST: api/[controller]
        [HttpPost]
        public async Task<ActionResult<TEntity>> Post(TEntity entity)
        {
            try
            {
                await repository.Add(entity);
                return CreatedAtAction("Post", new { id = entity.Id }, entity);
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
                throw;
            }
        }
        
        /// <summary>
        /// API Method to Delete one Entity.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deleted by id Entity or NotFoundResult.</returns>
        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TEntity>> Delete(int id)
        {
            var entity = await repository.Delete(id);
            if (entity == null)
            {
                return NotFound();
            }
            return entity;
        }

    }
}
