﻿using AutoMapper;
using Roofstock_challenge.core.DTOs;
using Roofstock_challenge.core.Models;

namespace Roofstock_challenge.MappingConfigurations
{
    /// <summary>
    /// User Profiles for Mapping purposes
    /// </summary>
    public class UserProfile : Profile
    {
        /// <summary>
        /// Constructor, here we define mappings between DTOs and Entities.
        /// </summary>
        public UserProfile()
        { 
            //Property entity mapping
            CreateMap<DTOProperty, Property>().ForMember(dest =>
            dest.Address,
            opt => opt.MapFrom(src => src.Address.GetFullAddress())).ForMember(dest =>
            dest.DTOId,
            opt => opt.MapFrom(src => src.Id)).ForMember(dest =>
            dest.ListPrice,
            opt => opt.MapFrom(src => src.Financial.ListPrice)).ForMember(dest =>
            dest.MonthlyRent,
            opt => opt.MapFrom(src => src.Financial.MonthlyRent)).ForMember(dest =>
            dest.YearBuilt,
            opt => opt.MapFrom(src => src.Physical.YearBuilt)).ForMember(dest => dest.Id, act => act.Ignore());
        }
    }
}
